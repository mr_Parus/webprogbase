var expressValidator = require('express-validator');

module.exports = createUrls;

/**
 * Set basic routes
 * @param app - express app
 */
function createUrls(app){
    app.use(expressValidator([]));

    app.use('/api/posts', require('./modules/posts/actions'));
    app.use('/api/users', require('./modules/users/actions'));
}