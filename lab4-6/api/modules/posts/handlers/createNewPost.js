var Post = require('./../../../models/post'),
    User = require('./../../../models/user'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = createNewPost;

/**
 * Create new post
 * @param postData - compleate post data
 * @returns {Promise<T>}
 */
function createNewPost(postData){
    var defer = q.defer(),
        error;

    new Post({
            title: postData.title,
            description: postData.description
        })
        .save()
        .then(function(){
        defer.resolve();
    }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });
    return defer.promise;
}
