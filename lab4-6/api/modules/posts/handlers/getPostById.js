var Post = require('./../../../models/post'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = getUserById;

/**
 * Find post by his id in db
 * @param postId - post id from req
 * @returns {Promise<T>}
 */
function getUserById(postId){
    var defer = q.defer(),
        error;

    Post
        .findOne({
            _id: postId
        })
        .populate({
            path: 'comments',
            populate: {
                path: 'authorId'
            }
        })
        .then(function(post){
            defer.resolve(post);
        }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}