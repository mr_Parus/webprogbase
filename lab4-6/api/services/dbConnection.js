var config = require('./../../config'),
    mongoose = require('mongoose'),
    log = require('./../services/log')(module),
    q = require('q'),
    db;

mongoose.Promise = q.Promise;
/**
 * Create new connection to db
 */
mongoose.connect(config.dbPath);
db = mongoose.connection;

/**
 * Db error listener
 */
db.on('error', function(){
    log.error("Can't connect to database.");
});

/**
 * Db on open connection listener, exec once
 */
db.once('open', function callback () {
    log.info("Connected to database.");
});

module.exports = mongoose;