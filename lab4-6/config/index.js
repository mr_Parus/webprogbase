module.exports = getConfig();

/**
 * Export configs
 */
function getConfig(){
    var env = process.env.NODE_ENV || "development";

    if(env == "production"){
        return require('./config-prod.json');
    } else {
        return require('./config-dev.json');
    }
}