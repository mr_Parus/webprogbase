var util = require('util');

/**
 * Custom error type for http errors
 * @param httpStatus - http status code
 * @param errorMsg - attached msg
 * @constructor
 */
function HttpError(httpStatus, errorMsg) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = errorMsg;
    this.httpStatus = httpStatus;
}

util.inherits(HttpError, Error);

/**
 * Overwrite toString method
 * @returns {ServerResponse|*}
 */
HttpError.prototype.toString = function(){
    return util.format("HTTP error: %d - %s \n %s", this.httpStatus, this.message, this.stack);
};

module.exports = HttpError;