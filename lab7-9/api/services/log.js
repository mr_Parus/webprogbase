var winston = require('winston'),
    config = require('./../../config');

/**
 * Create new logger
 * @param module - module var in some module for identing log msg
 * @returns {*}
 */
function getLogger(module) {
    var path = module.filename.split('/').slice(-2).join('/');

    return new winston.Logger({
        transports : [
            new winston.transports.Console({
                colorize:   true,
                level:      config.logLevel == 'development' ? 'debug' : 'error',
                label:      path
            })
        ]
    });
}

module.exports = getLogger;