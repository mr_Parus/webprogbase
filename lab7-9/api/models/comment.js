var mongoose = require('./../services/dbConnection'),
    log = require('./../services/log')(module),
    Post = require('./post'),
    Schema = mongoose.Schema,
    Comment = new Schema({
        text: {
            type: String,
            required: true
        },
        postId: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'Post'
        },
        authorId: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        date: { type: Date, default: Date.now }
    });


Comment.pre('remove', function (next) {
    var that = this;
    mongoose
        .model('Post')
        .findOne({_id: that.postId})
        .exec()
        .then(function (post) {
            var comments = post.comments,
                commentIndex = comments.indexOf(that._id);

            if (~commentIndex) {
                comments.splice(commentIndex, 1);
            }
            post
                .update({$set: {comments: comments}})
                .exec()
                .then(function () {
                    next();
                }, function (err) {
                    debugger;
                    log.error("Can't remove comment from post");
                });
        }, function (err) {
            log.error("Can't find post related to this comment");
        });
});

module.exports = mongoose.model("Comment", Comment);