var mongoose = require('./../services/dbConnection'),
    log = require('./../services/log')(module),
    Schema = mongoose.Schema,
    User = require('./user'),
    Comment = require('./comment'),
    Post = new Schema({
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        authorId: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        images: {
            type: [String],
            required: true
        },
        likes: {
            type: [{
                type: Schema.Types.ObjectId,
                ref: 'User'
            }],
            default: []
        },
        comments: {
            type: [{
                type: Schema.Types.ObjectId,
                ref: 'Comment'
            }],
            default: []
        },
        date: {
            type: Date,
            default: Date.now
        }
    });

Post.index({title: "text", description: "text"});

Post.pre('remove', function(next) {
    var that = this;
    Comment
        .where({_id: {$in: that.comments}})
        .remove()
        .exec()
        .then(function(){
            return  User
                .findOne({_id: that.authorId})
                .exec();
        }, function(err){
            log.error("Can't remove comments related to this post");
        })
        .then(function(author){
            var ownPosts = author.ownPosts,
                postIndex = ownPosts.indexOf(that._id);

            if(~postIndex){
                ownPosts.splice(postIndex, 1);
            }

            return author
                .update({$set: {ownPosts: ownPosts}})
                .exec();
        }, function(err){
            log.error("Can't remove postId from author own posts array");
        })
        .then(function(){
            return User
                .where({favorites: {$in: [that._id]}})
                .exec();
        }, function(err){
            log.error("Can't find users that added this post to favorites");
        })
        .then(function(userDocs){
            var i;
            if(!userDocs.length){
                next();
            } else {
                for(i = userDocs.length; i--;){
                    (function(author, i){
                        var favorites = author.favorites,
                            thisPostIndex = favorites.indexOf(that._id);
                        if(~thisPostIndex){
                            favorites.splice(thisPostIndex, 1);

                            author
                                .update({$set: {favorites: favorites}})
                                .exec()
                                .then(function(){
                                    if(!i)
                                        next();
                                }, function(err){
                                    log.error("Can't remove this postId from user favorites array");
                                });
                        }
                    })(userDocs[i], i);
                }
            }
        }, function(err){
            log.error("Huston, we have a problem!");
        });
});

module.exports = mongoose.model("Post", Post);
