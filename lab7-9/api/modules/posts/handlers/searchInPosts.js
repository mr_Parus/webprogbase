var Post = require('./../../../models/user'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = searchInPosts;

/**
 * Find post by his id in db
 * @param postId - post id from req
 * @returns {Promise<T>}
 */
function searchInPosts(searchStr){
    var defer = q.defer(),
        error;

    Post
        .find({username: searchStr})
        .then(function(posts){
            defer.resolve(posts);
        }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}