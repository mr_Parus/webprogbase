var Post = require('./../../../models/post'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');


module.exports = countPosts;

/**
 * Get all posts form db
 */
function countPosts(){
    var defer = q.defer(),
        error;
    Post
        .find({})
        .count(function(err, count){
        })
        .exec()
        .then(function(postsArr){
            defer.resolve(postsArr);
        }, function(err){
            error = new HttpError(503, 'No users');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}
