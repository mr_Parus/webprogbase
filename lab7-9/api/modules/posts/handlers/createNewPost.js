var Post = require('./../../../models/post'),
    User = require('./../../../models/user'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = createNewPost;

/**
 * Create new post
 * @param postData - compleate post data
 * @returns {Promise<T>}
 */
function createNewPost(postData){
    var defer = q.defer(),
        error;

    new Post({
            images: postData.images,
            authorId: postData.authorId,
            title: postData.title,
            description: postData.description
        })
        .save()
        .then(function(doc){
            User
                .where({ _id: postData.authorId })
                .update({$push: {ownPosts: doc._id}})
                .exec()
                .then(defer.resolve,
                    function(err){
                        error = new HttpError(503, 'Service unavailable');
                        log.error(error.toString());
                        defer.reject(error);
                    });
        }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });
    return defer.promise;
}
