module.exports = {
    postNewComment: require('./postNewComment'),
    deleteComment: require('./deleteComment')
};