var Post = require('./../../../models/user'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');


module.exports = countUsers;

/**
 * Get all posts form db
 */
function countUsers(){
    var defer = q.defer(),
        error;
    Post
        .find({})
        .count(function(err, count){
        })
        .exec()
        .then(function(postsArr){
            defer.resolve(postsArr);
        }, function(err){
            error = new HttpError(503, 'No users');
            log.error(error.toString());
            defer.reject(error);
        });
    return defer.promise;
}