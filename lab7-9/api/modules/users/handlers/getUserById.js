var User = require('./../../../models/user'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = getUserById;

/**
 * Find user by his id in db
 * @param userId - user id from req
 * @returns {Promise<T>}
 */
function getUserById(userId){
    var defer = q.defer(),
        error;

    User
        .findOne({
            _id: userId
        })
        .exec()
        .then(function(user){
            defer.resolve(user);
        }, function(err){
            error = new HttpError(404, 'User not found');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}