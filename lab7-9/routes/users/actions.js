var express = require('express'),
    router = express.Router(),
    config = require('./../../config/'),
    passport = require('passport'),
    handlers = require('./../../api/modules/users/handlers'),
    crypto = require('crypto');

module.exports = router;


/**
 * Sign error
 */
router.get('/register-error', function (req, res) {
    res.end("Register error")
});

/**
 * Sign up
 */
router.post('/register', function (req, res) {
    var formData = {
        password: req.body.password,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        gender: req.body.gender,
        username: req.body.username
    };
    if (!formData.username || !formData.password || formData.password !== req.body.confirm_password){
        debugger;
        res
            .status(200)
            .send({
            success: false,
            msg: "Неверное подтверждение пароля"
        });
        return ;
    }
    handlers.createUser(formData)
        .then(function(){
            res
                .status(200)
                .send({
                    success: true,
                    msg: "Успешная регистрация"
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    msg: err.message
                });
        });
});

/**
 * Sign in
 */
router.post('/login', passport.authenticate('local', {failureRedirect: '/login-err'}), function (req, res) {
    res.redirect("/")
});
/**
 * Sign in err
 */
router.get('/login-err', function (req, res) {
    res.send({
        success: false
    });
});

/**
 * Sign out
 */
router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

/**
 * Change user data
 */
router.post('/users/:ID',function(req, res){
    if (req.user&&req.user._id.toString() == req.params.ID.toString()){
        var formData ={};
        for ( data in req.body ){
            if (req.body.hasOwnProperty(data)){
                formData[data]= req.body[data];
            }
        }
        if (req.files){
            if (req.files.avatar) {
                formData.avatar = req.files.avatar[0].data.toString('base64');
            }
        }
        if (formData.confirm_password && formData.password && (formData.password != formData.confirm_password) ) {
            res.end("Password1 != password2");
        } else if (formData.password_last && hash(formData.password_last)!= req.user.password) {
            res.end("Your last password != your password");
        }
        handlers.changeUserData(req.params.ID, formData)
            .then(function(){
                res
                    .status(200)
                    .redirect('/users/'+req.user._id.toString());
                },function(err){
                res
                    .status(500)
                    .end("Changing error!");
            });
    }
    else
        res.end("You don't have access!");
});




function hash(password) {
    return crypto
        .createHash('md5')
        .update(password + config.hashSecret)
        .digest("hex");
}

