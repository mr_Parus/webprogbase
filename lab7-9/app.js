var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
var busboyBodyParser = require('busboy-body-parser'),///////////
    config = require('./config'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    User = require('./api/models/user'),
    crypto = require('crypto'),
    session = require('express-session');///////////////////////

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(busboyBodyParser({ multi: true }));////////////////////////////////////////////

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
///////////////////////////////////////////////////////////////////////////////////////
app.use(session({
    secret: config.sessionSalt,
    resave: false,
    saveUninitialized: true
}));

passport.serializeUser(function(user, done) {
    done(null, user._id);
});
passport.deserializeUser(function(id, done) {
    console.log("Deserializing User by id: " + id);
    User
        .findOne({_id: id})
        .exec()
        .then(function(user){
            if(user) {
                console.log("Sucsess deserializing User by id: " + id);
                done(null, user);
            } else {
                done("No user", null);
            }
        })
        .catch(function(err) {done(err, null)})
});

app.use(passport.initialize());
app.use(passport.session());
function hash(password) {
    return crypto
        .createHash('md5')
        .update(password + config.hashSecret)
        .digest("hex");
}
passport.use(new LocalStrategy(function (username, password, done) {
        console.log("Local: " + username + " : " + password);
    User
        .findOne({username: username,
            password: hash(password)})
        .exec()
        .then(function(user){
        if (user) {
             done(null, user);
        } else {
            console.log("No Registered!");
            done(null, false);
        }
        })
        .catch(function(err) {
            console.log(err);
            done(err, null);
        });
}));

////////////////////////////////////////////////////////////////////////////////////////
/**
 * Set API routes
 */
require('./api/apiRoutes')(app);
/**
 * Set Client routes
 */
require('./routes/Routes')(app);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
